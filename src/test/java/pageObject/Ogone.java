package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ogone {
	
public static WebElement element=null;
	
	public static WebElement CardNumber(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='Ecom_Payment_Card_Number']"));
		
		}catch(Exception e) {
			System.out.println("L'élément CardNumber n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement PayPal_Simulator(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@id='ACS']/h1"));
		
		}catch(Exception e) {
			System.out.println("L'élément PayPal_Simulator n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}

	public static WebElement SelectMonth(WebDriver driver) throws Exception {
	
		try {
		element=driver.findElement(By.xpath("//select[@id='Ecom_Payment_Card_ExpDate_Month']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SelectMonth n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement SelectYear(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//select[@id='Ecom_Payment_Card_ExpDate_Year']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SelectYear n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement CVC(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='Ecom_Payment_Card_Verification']"));
		
		}catch(Exception e) {
			System.out.println("L'élément CVC n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Confirm_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='submit3']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Confirm_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}

}

