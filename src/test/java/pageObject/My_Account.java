package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class My_Account {
	
public static WebElement element=null;
	
	public static WebElement Account_Heading(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//h1[@class='account__heading']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Account_Heading n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}

}

