package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Payement {
	
public static WebElement element=null;
	
	public static WebElement SeeAdresses_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@class='btns bdrred arrowr margin-sm-rigt']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SeeAdresses_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Bloc_address(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//a[@class='adress-checkout']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Bloc_address n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Save_btn(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"formCorrectAddressBilling\"]/div[4]/button"));
		
		}catch(Exception e) {
			System.out.println("L'élément Save_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Obligatory_CGV(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='termsAndConditions']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_CGV n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement CGV_Link(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//a[@title='Conditions Générales de Vente']")); 
		
		}catch(Exception e) {
			System.out.println("L'élément CGV_Link n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement CGV_Page(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"Services-Conseil\"]/div"));
		
		}catch(Exception e) {
			System.out.println("L'élément CGV_Page n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement PayementMethods(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//ul[@class='list-paiement-methods form-group form-group-checkradio has-feedback']"));
		
		}catch(Exception e) {
			System.out.println("L'élément PayementMethods n'a pas été trouvé.");
			throw(e);
		}
		return element; 
		}
	
	
	public static WebElement CGV_CheckBox(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"formPayment\"]/div/div[2]/div[5]/div[1]/div/div"));
		
		}catch(Exception e) {
			System.out.println("L'élément CGV_CheckBox n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Validate_btn(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//div[@class='pull-right']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Validate_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement OrderConfirmation(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//div[@class='bloc_confirmation_message text-center']"));
		
		}catch(Exception e) {
			System.out.println("L'élément OrderConfirmation n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}

}
