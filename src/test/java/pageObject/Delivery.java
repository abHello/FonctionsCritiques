package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Delivery {
public static WebElement element=null;
	
	public static WebElement SeeAdresses_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@class='btns bdrred arrowr red']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SeeAdresses_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement AddAdresse_btn(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//a[@class='btns bdrred arrowr']"));
		
		}catch(Exception e) {
			System.out.println("L'élément AddAdresse_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement AdressName(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='address_AddressName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément AdressName n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement CompanyName(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='address_CompanyName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément CompanyName n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement NVoie(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='address_Address1']"));
		
		}catch(Exception e) {
			System.out.println("L'élément NVoie n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement PostalCode(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='address_PostalCode']"));
		
		}catch(Exception e) {
			System.out.println("L'élément PostalCode n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement City(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='address_City']"));
		
		}catch(Exception e) {
			System.out.println("L'élément City n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_FirstName(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ContactAddressForm_FirstName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_FirstName n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_FLastName(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ContactAddressForm_LastName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_FLastName n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_Email(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ContactAddressForm_Email']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_Email n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_Phone(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ContactAddressForm_PhoneHome']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_Phone n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement SubmitAddress(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@name='createAddress']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SubmitAddress n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement ReturnToDelivery_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@class='btns red arrowr']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SubmitAddress n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Bloc_address(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//a[@class='bloc-adress-checkout']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Bloc_address n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Adresses_group(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//div[@class='form-group form-group-checkradio last has-feedback']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Adresses_group n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Validate_btn(WebDriver driver) throws Exception { 
		
		try {
		element=driver.findElement(By.xpath("//button[@class='btns arrowr red']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Validate_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_select(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='form-group form-group-checkradio last has-feedback']/div[1]"));
		
		}catch(Exception e) {
			System.out.println("L'élément Validate_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contacts_group(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='modal_select_address-result']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contacts_group n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='listContainer__link--content']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contacts n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement AddContact_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@class='btns bdrred arrowr margin-sm-rigt']"));
		
		}catch(Exception e) {
			System.out.println("L'élément AddContact n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement AddContact_link(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='links grey m-b-10 add-contact']"));
		
		}catch(Exception e) {
			System.out.println("L'élément AddContact_link n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_new_FirstName(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ContactAddressForm_new_FirstName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_new_FirstName n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_new_LastName(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ContactAddressForm_new_LastName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_new_LastName n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_new_Email(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ContactAddressForm_new_Email']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_new_Email n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Contact_new_Phone(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ContactAddressForm_new_PhoneHome']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_new_Phone n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Save_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"formCorrectAddressShipping\"]/div[4]/button"));
		
		}catch(Exception e) {
			System.out.println("L'élément Contact_new_Phone n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Edit_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@role='button']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Edit_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement ShippingMode_list(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//ul[@class='list-shipping-methods form-group form-group-checkradio']"));
		
		}catch(Exception e) {
			System.out.println("L'élément ShippingMode_list n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Raja_Drive(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@id='address-drive']"));
		
		}catch(Exception e) {
			System.out.println("L'élément ShippingMode_list n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement DisplayAll_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@class='btns bdrwhite small more']"));
		
		}catch(Exception e) {
			System.out.println("L'élément DisplayAll_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement ValidateShipping_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@class='btns red arrowr']"));
			//element=driver.findElement(By.xpath("//button[@class='btns arrowr red fright']"));
		
		}catch(Exception e) {
			System.out.println("L'élément ValidateShipping_btn n'a pas été trouvé.");
			throw(e);
		}
		return element; 
		}

}
