package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PDT {
	
public static WebElement element=null;
	
	public static WebElement Picture(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='pv__media']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Image n'a pas été trouvé dans PDT.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Titre(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='col-xs-12']/h1"));
		
		}catch(Exception e) {
			System.out.println("L'élément Titre n'a pas été trouvé dans PDT.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Text(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='block__cms pv__list m-b-20']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Texte n'a pas été trouvé dans PDT.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement See_TablePrice(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@class='btns red arrowr']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Table_Price n'a pas été trouvé dans PDT.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Quantity_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@class='ui-spinner-button ui-spinner-up ui-corner-tr ui-button ui-widget ui-state-default ui-button-text-only']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Quantité n'a pas été trouvé dans PDT.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Quantity_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@data-import='ui.spinner']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Quantité n'a pas été trouvé dans PDT.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Table_Price(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//table[@data-import='sticky.sort']"));
		
		}catch(Exception e) {
			System.out.println("L'élément table n'a pas été trouvé dans PDT.");
			throw(e);
		}
		return element;
		}

}
