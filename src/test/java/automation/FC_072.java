package automation;

import org.testng.annotations.Test;

import appModule.Order;
import pageObject.Payement;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_072 {
	WebDriver driver=null;
	
  @Test
  public void C_072() throws Exception {
	  try {
		Order.DoOrder(driver, 0);
		
		Assert.assertTrue(Payement.OrderConfirmation(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("erreur : blocage au niveau de la page Ogone.");
		throw(e);
	}
  }

  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}
  

}
