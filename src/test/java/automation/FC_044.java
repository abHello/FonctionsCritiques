package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Prices;
import pageObject.Basket;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_044 {
	WebDriver driver=null;
	
	  @Test
	  public void C_044() throws Exception {
		  try {
			  BasketLanding.Basket(driver); 
			  Basket.AddQuantity(driver).click();
			  Thread.sleep(1000);
			  Assert.assertEquals(Prices.TotalProduct_HT(driver), Prices.UnitPrice(driver)*2);
			  
		} catch (Exception e) {
			System.out.println("erreur : Prix total n'est pas changé quand la quantité est modifiée.");
			throw(e);
		}	  
	  }

	  @BeforeTest
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterTest
	  public void afterTest() {
				  	driver.quit();
					}

}
