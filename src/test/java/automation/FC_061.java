package automation;

import org.testng.annotations.Test;

import appModule.PayementLanding;
import junit.framework.Assert;

import pageObject.Delivery;
import pageObject.Payement;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class FC_061 {
	WebDriver driver=null;
	String str=null;
	
  @Test
  public void C_055() throws Exception {
	  try {
		  
		  PayementLanding.Payement(driver);
		  Thread.sleep(1000);
		  Delivery.ValidateShipping_btn(driver).click();
		  Payement.Bloc_address(driver).click();
		  str=Delivery.AdressName(driver).getAttribute("value");
		  
		  Delivery.AdressName(driver).clear();
		  Delivery.AdressName(driver).sendKeys(str+"a");
		  
		  Payement.Save_btn(driver).click();
		  Delivery.ReturnToDelivery_btn(driver).click();
		  
		  Payement.Bloc_address(driver).click();
		  
		  Assert.assertTrue(Delivery.AdressName(driver).getAttribute("value").equals(str+"a"));
		  
		  Delivery.AdressName(driver).clear();
		  Delivery.AdressName(driver).sendKeys(str);
		  Payement.Save_btn(driver).click();
		  Delivery.ReturnToDelivery_btn(driver).click();
	  }catch(Exception e) {
		  System.out.println("erreur : adresse de facturation ne peut être modifiée.");
		  throw(e);
	  }
  }

  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			//driver.quit();
 					}

}
