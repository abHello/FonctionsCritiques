package automation;

import org.testng.annotations.Test;

import appModule.ConnectTo_RajaMail;
import appModule.Order;
import pageObject.MailBoxRaja;
import pageObject.Payement;
import utility.CompareHours;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_071_fr {
	
	WebDriver driver=null;
	int hour1,minute1=0;  
	Calendar cal=null;  
	
  @Test
  public void C_071_fr() throws Exception {
	  try {  
		  cal = Calendar.getInstance();
	      hour1 = cal.get(Calendar.HOUR_OF_DAY);
	      minute1 = cal.get(Calendar.MINUTE);
	      
		  Order.DoOrder(driver, 6);
		  
		  Assert.assertTrue(Payement.OrderConfirmation(driver).isDisplayed());

	      ConnectTo_RajaMail.login(driver, Data.Email_Raja, Data.Password_Raja);

	      CompareHours.Compare(hour1+":"+minute1, MailBoxRaja.Time(driver).getText());
	        
	      Assert.assertTrue(MailBoxRaja.Subject(driver).getText().contains("Confirmation de commande"));
		
	} catch (Exception e) {
		System.out.println("erreur : La confirmation de commande ne s'affiche pas quand 'A l'échéance' est choisi. / Email n'est pas envoyé au CRC.");
		throw(e);
	}
  }

  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			//driver.quit();
 					}

}
