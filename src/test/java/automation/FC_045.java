package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import pageObject.Basket;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_045 {
	WebDriver driver=null;
	List<WebElement> lst=null;
	
	  @Test
	  public void C_045() throws Exception {
		  try {
			  BasketLanding.Basket(driver); 
			  lst=Basket.Table_Ref(driver).findElements(By.tagName("td"));
			  lst.get(5).click();
			  Thread.sleep(1000);
			  Assert.assertFalse(lst.get(0).getText().contains(Data.Commande_Ref));
			  
		} catch (Exception e) {
			System.out.println("suppression erreur : article n'est pas supprimé du panier.");
			throw(e);
		}	  
	  }

	  @BeforeTest
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterTest
	  public void afterTest() {
				  	driver.quit();
					}

}
