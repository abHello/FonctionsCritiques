package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.RegistrationForm;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_031 {
	WebDriver driver=null;
	
	  @Test
	  public void C_031() throws Exception {
			  try {
				Home.MyAccount_btn(driver).click();
				try { 
					if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
						Login_Page.FirstCommande_Alert(driver).click();
					}}catch(Exception e) {
					}
				Login_Page.Register(driver).click();
				RegistrationForm.Particular(driver).click();
				RegistrationForm.Company_box(driver).click();
				Assert.assertTrue(RegistrationForm.CompanyName_fld(driver).isDisplayed());
				Assert.assertTrue(RegistrationForm.Siret_fld(driver).isDisplayed());
				Assert.assertTrue(RegistrationForm.CheckSiret_box(driver).isDisplayed());
			} catch (Exception e) {
				System.out.println("Erreur: champs company, siret ne s'affichent pas quand compte entreprise est coché.");
				throw(e);
			}
		  }

	  @BeforeTest
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterTest
	  public void afterTest() {
				  	driver.quit();
					}

}
