package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_076_fr {
	WebDriver driver=null;
	
  @Test
  public void C_076_fr() throws Exception {
	  try {
		Home.LandingPage(driver).click();
		Assert.assertTrue(driver.getCurrentUrl().contains(Data.UserAgent_LP_fr));
	} catch (Exception e) {
		System.out.println("erreur configuration apache : user agent cms_000073 n'est pas appelé dans lea landing page pour FR.");
		throw(e);
	}
  }


  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}


}
