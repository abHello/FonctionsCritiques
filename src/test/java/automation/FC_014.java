package automation;

import org.testng.annotations.Test;

import appModule.PDT_Landing;
import pageObject.PDT;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class FC_014 {
	WebDriver driver=null;
	List<WebElement> TrNumber=null;
	Object[] col=null;
	WebElement TdNumber=null;
	
  @Test
  public void C_014() throws Exception {
	  try {
		
		PDT_Landing.PDT(driver);
		PDT.See_TablePrice(driver).click();

		/*TrNumber=PDT.Table_Price(driver).findElements(By.tagName("td"));
		TdNumber=TrNumber.get(12).findElement(By.tagName("a"));
		TdNumber.click();
		TdNumber=TrNumber.get(12).findElement(By.tagName("input"));
		TdNumber.get*/
		//col=TrNumber.toArray();
		
		//System.out.println(TdNumber.getAttribute("data-spinner-step"));
	
	} catch (Exception e) {
		System.out.println("Le titre n'est pas affiché dans PDT.");
		throw(e);
	}
  }

  @BeforeTest
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterTest
  public void afterTest() {
	  	driver.quit();
  		}

}
