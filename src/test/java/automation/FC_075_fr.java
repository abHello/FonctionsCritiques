package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_075_fr {
	WebDriver driver=null;
	List<WebElement> lst=null;
	
  @Test
  public void C_075_fr() throws Exception {
	  try {
		lst=Home.ContactUS(driver).findElements(By.tagName("li"));
		lst.get(2).click();
		
		Assert.assertTrue(driver.getCurrentUrl().contains(Data.UserAgent_ContactForm_fr));
	} catch (Exception e) {
		System.out.println("erreur configuration apache : user agent cms_000032 n'est pas appelé dans le formulaire de contact pour FR.");
		throw(e);
	}
  }


  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
