package automation;

import org.testng.annotations.Test;

import appModule.AddPayementAddress;

import appModule.PayementLanding;

import pageObject.Delivery;
import pageObject.Payement;
import utility.AddressesNumber;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_060 {
	
	WebDriver driver=null;
	int nbAdd=0;

  @Test
  public void C_060() throws Exception { 
	  try {
		  PayementLanding.Payement(driver);
		  
		  nbAdd=AddressesNumber.Number(Payement.SeeAdresses_btn(driver).getText());
		  Thread.sleep(1000);
		  Payement.SeeAdresses_btn(driver).click();
		  Delivery.AddAdresse_btn(driver).click();
		  AddPayementAddress.AddAddress(driver);
		  
		  Assert.assertEquals(AddressesNumber.Number(Payement.SeeAdresses_btn(driver).getText()), nbAdd+1);
		  
	} catch (Exception e) {
		System.out.println("erreur : adresse de facturation n'est pas créée.");
		throw(e);
	}
  }

  @BeforeTest
  public void beforeTest() { 
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
