package automation;

import org.testng.annotations.Test;

import appModule.PDT_Landing;
import pageObject.PDT;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_011 {
	WebDriver driver=null;
	
  @Test
  public void C_011() throws Exception {
	  try {
		PDT_Landing.PDT(driver);
		Assert.assertTrue(PDT.Picture(driver).isDisplayed());
		
	} catch (Exception e) {
		System.out.println("L'image n'est affiché dans PDT.");
		throw(e);
	}
  }

  @BeforeTest
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterTest
  public void afterTest() {
	  	driver.quit();
  		}

}
