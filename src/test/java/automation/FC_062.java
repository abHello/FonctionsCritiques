package automation;

import org.testng.annotations.Test;

import appModule.PayementLanding;
import junit.framework.Assert;

import pageObject.Delivery;
import pageObject.Payement;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class FC_062 {
	WebDriver driver=null;
	
  @Test
  public void C_062() throws Exception {
	  try {
		  PayementLanding.Payement(driver);
		  Delivery.ValidateShipping_btn(driver).click();
		  Delivery.ValidateShipping_btn(driver).click();
		  Assert.assertTrue(Payement.Obligatory_CGV(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("erreur : Message d'erreur non affiché quand CGV n'est pas cochée.");
		throw(e);
	}

  }


  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
