package automation;

import org.testng.annotations.Test;

import appModule.Order;
import pageObject.Payement;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_067_fr {
	WebDriver driver=null;
	
  @Test
  public void C_067_fr() throws Exception {
	  try {
		  Order.DoOrder(driver, 2);
		  
		  Assert.assertTrue(Payement.OrderConfirmation(driver).isDisplayed());
	  
	} catch (Exception e) {
		System.out.println("erreur : La confirmation de commande ne s'affiche pas quand 'Virement' est choisi.");
		throw(e);
	}
  }

  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
@AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
