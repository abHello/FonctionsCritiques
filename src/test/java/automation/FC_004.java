package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_004 {
	WebDriver driver=null;
	
  @Test
  public void C_004() throws Exception {
	  try {
		Assert.assertTrue(Home.SearchTerm(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("Le moteur de recherche de la home n'est pas affiché");
		throw(e);
	}
  }
  

  @BeforeTest
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterTest
  public void afterTest() {
	  	driver.quit();
  		}

}
