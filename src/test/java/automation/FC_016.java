package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_016 {
	WebDriver driver=null;
	
  @Test
  public void C_016() throws Exception{
		try {
			Home.MyAccount_btn(driver).click();
			try { 
				if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
					Login_Page.FirstCommande_Alert(driver).click();
				}}catch(Exception e) {
				}
			Login_Page.Login_btn(driver).click();
			Login_Page.Email(driver).sendKeys(Data.Email);
			Login_Page.Password(driver).sendKeys(Data.Fake_Password);
			Login_Page.Submit(driver).click();
			Assert.assertTrue(Login_Page.AlertFakeLogin(driver).isDisplayed());
		}catch(Exception e) {
			System.out.println("Faux login à un compte ne fonctionne pas : email valide, mot de passe invalide.");
			throw(e); 
		}
}

  @BeforeTest
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterTest
  public void afterTest() {
	  	//driver.quit();
  		}
}
