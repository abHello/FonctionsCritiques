package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_077 {
	WebDriver driver=null;
	
  @Test
  public void C_077() throws Exception {
	  try {
		Assert.assertTrue(Home.Robots(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("erreur robots : robots.txt n'est pas présent au niveau du serveur.");
		throw(e);
	}
  }


  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url_Robots);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
