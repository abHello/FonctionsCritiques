package automation;

import org.testng.annotations.Test;

import appModule.PayementLanding;
import pageObject.Delivery;
import pageObject.Ogone;
import pageObject.Payement;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_066_fr {
	WebDriver driver=null;
	List<WebElement> lst=null;
	Actions actions=null;
	
  @Test
  public void C_066_fr() throws Exception {
	  try {
		  PayementLanding.Payement(driver);
		  Delivery.ValidateShipping_btn(driver).click();
		  lst=Payement.PayementMethods(driver).findElements(By.tagName("li"));
		  lst.get(0).click();
		  
		  actions = new Actions(driver);
		  actions.moveToElement(Payement.CGV_CheckBox(driver)).click().build().perform();
		  Payement.Validate_btn(driver).click();
		  
		  Assert.assertTrue(Ogone.CardNumber(driver).isDisplayed());
		  
	} catch (Exception e) {
		System.out.println("erreur : La page ogone ne s'affiche pas quand 'CB en ligne' est choisie.");
		throw(e);
	}

  }

  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
@AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
