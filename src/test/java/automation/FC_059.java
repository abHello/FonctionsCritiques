package automation;

import org.testng.annotations.Test;

import appModule.PayementLanding;
import junit.framework.Assert;

import pageObject.Delivery;
import pageObject.Payement;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class FC_059 {
	WebDriver driver=null;
	String str=null;
	List<WebElement> lst=null;
	
  @Test
  public void C_059() throws Exception {
	  try {
		  PayementLanding.Payement(driver);
		  Thread.sleep(1000);
		  Payement.SeeAdresses_btn(driver).click();
		  lst=Delivery.Adresses_group(driver).findElements(By.tagName("div"));

		  str=lst.get(1).getText();
		  lst.get(1).click(); 
		  Delivery.Validate_btn(driver).click();
		  
		  Assert.assertEquals(str.substring(0,10), Payement.Bloc_address(driver).getText().substring(0,10));
	} catch (Exception e) {
		System.out.println("erreur : adresse de paiement n'est pas séléctionnée.");
		throw(e);
	}

  }

  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
