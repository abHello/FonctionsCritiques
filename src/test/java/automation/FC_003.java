package automation;

import org.testng.annotations.Test;

import junit.framework.Assert;
import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class FC_003 {
	WebDriver driver=null;
  @Test
  public void C_003() throws Exception {
	  try {
		Assert.assertTrue(Home.Navigation(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("La navigation de la home n'est pas affiché");
		throw(e);
	}
  }


  @BeforeTest
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS); 
		driver.get(Data.url);
  		}
  @AfterTest
  public void afterTest() {
	  	driver.quit();
  		}

}
