package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.ObligatoryInformation;
import pageObject.RegistrationForm;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_024 {
	WebDriver driver=null;
	
  @Test
  public void C_024() throws Exception {
	  try {
		Home.MyAccount_btn(driver).click();
		try { 
			if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
				Login_Page.FirstCommande_Alert(driver).click();
			}}catch(Exception e) {
			}
		Login_Page.Register(driver).click();
		
		RegistrationForm.Submit_btn(driver).click();
		Assert.assertTrue(ObligatoryInformation.Obligatory_Mail(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_Password(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_Company(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_Title(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_FirstName(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_LastName(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_PhoneHome(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_Mobile(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_Adresse1(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_PostalCode(driver).isDisplayed());
		Assert.assertTrue(ObligatoryInformation.Obligatory_City(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("Bouton 'creer compte' ne fonctionne pas.");
		throw(e);
	}
	  
	  
  }

	@BeforeTest
	public void beforeTest() {
		System.setProperty(Data.DriverProperty, Data.ChromeDriver);
			driver=new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.get(Data.url);
			}
	@AfterTest
	public void afterTest() {
		  	driver.quit();
			}

}
