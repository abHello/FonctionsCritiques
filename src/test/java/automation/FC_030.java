package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.RegistrationForm;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_030 {
	WebDriver driver=null;
	
	  @Test
	  public void C_030() throws Exception {
			  try {
				Home.MyAccount_btn(driver).click();
				try { 
					if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
						Login_Page.FirstCommande_Alert(driver).click();
					}}catch(Exception e) {
					}
				Login_Page.Register(driver).click();
				
				RegistrationForm.Particular(driver).click();
				Assert.assertFalse(RegistrationForm.CompanyName_fld(driver).isDisplayed());
				Assert.assertFalse(RegistrationForm.Siret_fld(driver).isDisplayed());
				Assert.assertFalse(RegistrationForm.CheckSiret_box(driver).isDisplayed());
			} catch (Exception e) {
				System.out.println("Erreur: champs company, siret ne disparaissent pas quand compte particulier est coché.");
				throw(e);
			}
		  }

	  @BeforeTest
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterTest
	  public void afterTest() {
				  	driver.quit();
					}

}
