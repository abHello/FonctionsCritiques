package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Login;
import pageObject.Basket;
import pageObject.Delivery;
import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_050 {
	WebDriver driver=null; 

 @Test
 public void C_050() throws Exception {
		  try { 
			  Home.MyAccount_btn(driver).click();
			  Login.login(driver);
			  BasketLanding.Basket(driver); 
			  Basket.DoOrder_btn(driver).click();
			  Assert.assertTrue(Delivery.SeeAdresses_btn(driver).isDisplayed());
			  
		} catch (Exception e) {
			System.out.println("erreur livraison : bouton 'voir carnet d'adresses de livraison' n'est pas affiché."); 
			throw(e);
		}	  
	  }

 @BeforeTest
 public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
			driver=new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.get(Data.url);
					}
 @AfterTest
 public void afterTest() {
			driver.quit();
					}

}
