package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import pageObject.Basket;
import pageObject.Login_Page;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_049 {
	WebDriver driver=null; 

 @Test
 public void C_049() throws Exception {
		  try {
			  BasketLanding.Basket(driver); 
			  Basket.DoOrder_btn(driver).click();
			  Assert.assertTrue(Login_Page.Login_btn(driver).isDisplayed());
		} catch (Exception e) {
			System.out.println("erreur panier : page de connexion au compte ne s'affiche pas.");
			throw(e);
		}	  
	  }

 @BeforeTest
 public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
 @AfterTest
 public void afterTest() {
				  	driver.quit();
					}

}
