package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Login;
import pageObject.Basket;
import pageObject.Delivery;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_057 {
	WebDriver driver=null;
	List<WebElement> lst=null;
	
  @Test
  public void C_057() throws Exception {
	  try {
		  Thread.sleep(1000);
		  BasketLanding.Basket(driver);
		  Basket.DoOrder_btn(driver).click();
		  Login.login(driver);
		  Thread.sleep(1000);
		  Delivery.SeeAdresses_btn(driver).click();
		  Delivery.DisplayAll_btn(driver).click();
		  
		  Thread.sleep(1000);
		  lst=Delivery.Adresses_group(driver).findElements(By.tagName("div"));
		  
		  
		  for(int i=0; i<lst.size(); i++) {
			  if(lst.get(i).getText().contains("Corse")) {
				  lst.get(i).click();
				  break;
			  }}

		  Delivery.Validate_btn(driver).click();
		  lst=Delivery.Adresses_group(driver).findElements(By.tagName("div"));
		  lst.get(0).click();
		  Delivery.Validate_btn(driver).click();
		  
		  lst=Delivery.ShippingMode_list(driver).findElements(By.tagName("li"));
		  Assert.assertTrue(lst.get(0).getText().contains("CORSE")); 
		  Assert.assertTrue(lst.size()==1);
		  
	  }catch(Exception e) {
		  System.out.println("erreur : méthode de livraison CORSE n'est pas présente pour une adresse corse.");
		  throw(e);
	  }
  }
 
  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
