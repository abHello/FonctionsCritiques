package automation;

import org.testng.annotations.Test;

import appModule.PayementLanding;
import pageObject.Basket;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_064 {
	WebDriver driver=null;
	
  @Test
  public void C_064() throws Exception {
	  try {
		  PayementLanding.Payement(driver);
		  
		  Assert.assertTrue(Basket.TablePrice_Recap(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("erreur : Le récap des prix n'est pas présent.");
		throw(e);
	}

  }

  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
@AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
