package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.RegistrationForm;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_034 {
	WebDriver driver=null;
	
	  @Test
	  public void C_034() throws Exception {
			  try {
				Home.MyAccount_btn(driver).click();
				try { 
					if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
						Login_Page.FirstCommande_Alert(driver).click();
					}}catch(Exception e) {
					}
				Login_Page.Register(driver).click();
				RegistrationForm.PostalCode_fld(driver).sendKeys(Data.PostalCode_more5_FR);
				RegistrationForm.Submit_btn(driver).click();
				Assert.assertTrue(RegistrationForm.PostalCode_fld(driver).getAttribute("value").equals(Data.PostalCode_FR));
			} catch (Exception e) {
				System.out.println("Code postal erroné: code postal n'est pas tronqué.");
				throw(e);
			}
		  }

	  @BeforeTest
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterTest
	  public void afterTest() {
				  	driver.quit();
					}

}
