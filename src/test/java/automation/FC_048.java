package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Prices;
import pageObject.Basket;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_048 {
	WebDriver driver=null;
	int i=0;
	  @Test
	  public void C_048() throws Exception {
		  try {
			  Thread.sleep(1000);
			  BasketLanding.Basket(driver); 

			  while((Prices.TotalProduct_HT(driver)<200) && (i<200)) {
				  Basket.AddQuantity(driver).click();
				  Thread.sleep(2000);
				  i++;
			  }

			  Assert.assertEquals(Prices.Total_HT_freeShipping(driver), Prices.TotalProduct_HT(driver)+Prices.RajaGarantie_freeShipping(driver));
			  
		} catch (Exception e) {
			System.out.println("erreur : La livraison n'est pas offerte si la commande dépasse 200 euros.");
			throw(e);
		}	  
	  }

	  @BeforeTest
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterTest
	  public void afterTest() {
				  	driver.quit();
					}

}
