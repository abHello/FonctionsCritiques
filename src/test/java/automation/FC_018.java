package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_018 {
	WebDriver driver=null;
	
  @Test
  public void C_018() throws Exception{
		try {
			Home.MyAccount_btn(driver).click();
			try { 
				if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
					Login_Page.FirstCommande_Alert(driver).click();
				}}catch(Exception e) {
				}
			Login_Page.Login_btn(driver).click();
			Login_Page.Email(driver).sendKeys("");
			Login_Page.Submit(driver).click();
			Assert.assertFalse(Login_Page.Disabled_submit(driver).isEnabled());
		}catch(Exception e) {
			System.out.println("Bouton valider n'est pas grisé en cas de email ou password vides.");
			throw(e);
		}
}

@BeforeTest
public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
		}
@AfterTest
public void afterTest() {
	  	driver.quit();
		}

}
