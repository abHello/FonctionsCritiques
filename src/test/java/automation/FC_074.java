package automation;

import org.testng.annotations.Test;

import appModule.ConnectTo_GMail;
import appModule.Order;
import pageObject.MailBox;
import utility.CompareHours;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_074 {
	WebDriver driver=null;
	int hour1,minute1=0;  
	Calendar cal=null; 
	List<WebElement> lst=null;
	
  @Test
  public void C_074() throws Exception {
	  try {
		  cal = Calendar.getInstance();
	      hour1 = cal.get(Calendar.HOUR_OF_DAY);
	      minute1 = cal.get(Calendar.MINUTE);
	      
		  Order.DoOrder(driver, 0);
		  
		  ConnectTo_GMail.login(driver, Data.Email, Data.Password);
		  
		  lst=MailBox.Email_Table(driver).findElements(By.tagName("td"));

		  CompareHours.Compare(hour1+":"+minute1, lst.get(7).getText());
		  
		  Assert.assertTrue(lst.get(5).getText().equals("Service client Raja"));
		  
	  } catch (Exception e) {
			System.out.println("erreur : Email n'est pas envoyé au client après achat par carte bancaire.");
			throw(e);
		}
	  
  }


  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterTest
  public void afterTest() {
 			driver.quit();
 					}

}
