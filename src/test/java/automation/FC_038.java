package automation;

import org.testng.annotations.Test;

import appModule.Registration;
import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_038 {
	WebDriver driver=null;
	
  @Test
  public void C_038() throws Exception {
	  try {
		Registration.Account_Creation(driver);
		Assert.assertTrue(Home.MyAccount_Connected(driver).getText().contains(Registration.email.substring(0, 10)));
	} catch (Exception e) {
		System.out.println("Problème dans la création du compte e-shop FR.");
		throw(e);
	}
  }

  @BeforeTest
  public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
				driver=new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				driver.get(Data.url);
				}
  @AfterTest
  public void afterTest() {
			  	driver.quit();
				}

}
