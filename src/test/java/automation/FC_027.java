package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.ObligatoryInformation;
import pageObject.RegistrationForm;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_027 {
	WebDriver driver=null;
	
	  @Test
	  public void C_027() throws Exception {
		  try {
			Home.MyAccount_btn(driver).click();
			try { 
				if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
					Login_Page.FirstCommande_Alert(driver).click();
				}}catch(Exception e) {
				}
			Login_Page.Register(driver).click();
			RegistrationForm.Password_fld(driver).sendKeys(Data.Password_Less5);
			RegistrationForm.Submit_btn(driver).click();
			Assert.assertTrue(ObligatoryInformation.Incorrect_Password(driver).isDisplayed());

		} catch (Exception e) {
			System.out.println("message erreur : 'password contenant moins que 5 caractères' n'est pas affiché.");
			throw(e);
		}
	  }

		@BeforeTest
		public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
				driver=new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				driver.get(Data.url);
				}
		@AfterTest
		public void afterTest() {
			  	driver.quit();
				}

}
