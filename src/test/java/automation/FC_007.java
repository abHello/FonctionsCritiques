package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_007 {
	WebDriver driver=null;
	List<WebElement> MenuNumber=null; 
	
@Test
  public void C_007() throws Exception {
	  try {
		MenuNumber=Home.BigMenu_lst(driver).findElements(By.tagName("li"));
		MenuNumber.get(0).click();
		MenuNumber=Home.Univers_menu(driver).findElements(By.tagName("li"));
		
		Assert.assertNotNull(MenuNumber.get(0).getText());
		Assert.assertTrue(MenuNumber.get(0).findElement(By.xpath(Data.Img_Xpath)).isDisplayed());

	} catch (Exception e) {
		System.out.println("L'image et le titre de la sous famille ne s'affichent pas correctement.");
		throw(e);
	}

	  
  }


  @BeforeTest
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterTest
  public void afterTest() {
	  	driver.quit();
  		}

}
