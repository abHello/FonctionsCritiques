package automation;

import org.testng.annotations.Test;

import appModule.Order;
import pageObject.Ogone;

import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_069_fr {
	WebDriver driver=null;
	
  @Test
  public void C_069_fr() throws Exception {
	  try {
		  Order.DoOrder(driver, 4);
		  
		  Assert.assertTrue(Ogone.PayPal_Simulator(driver).isDisplayed());
		
	} catch (Exception e) {
		System.out.println("erreur : La page PayPal ne s'affiche pas.");
		throw(e);
	}
  }

  @BeforeTest
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
@AfterTest
  public void afterTest() {
 			driver.quit();
 					} 

}
