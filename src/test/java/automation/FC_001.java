package automation;

import org.testng.annotations.Test;


import pageObject.Home;
import utility.Data;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class FC_001 {
	WebDriver driver=null;
	List<WebElement> MenuNumber=null;
	
  @Test
  public void C_001() throws Exception {

	    
	    try {
	    	System.setProperty(Data.DriverProperty, Data.ChromeDriver); 
			driver=new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS); 
			driver.get(Data.url);
			
			MenuNumber=Home.BigMenu_lst(driver).findElements(By.tagName("li")); 
			Assert.assertTrue(MenuNumber.size()==13);
			
		} catch (Exception e) {
			System.out.println("Le big menu ne s'affiche pas correctement");
			throw(e);
		}
	    
	  
  }

  
  //@BeforeTest
  //public void beforeTest() {

	//System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		//driver=new ChromeDriver();
	    
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS); 
		//driver.get(Data.url);
  		//}
  //@AfterTest
  //public void afterTest() {
	  	//driver.quit();
  		//}

}
