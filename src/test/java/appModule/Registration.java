package appModule;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.RegistrationForm;
import utility.Data;
import utility.RandomEmail;

public class Registration {
	public static String email=RandomEmail.emailString();
	
	public static void Account_Creation(WebDriver driver) throws Exception{
		
		try {
			Home.MyAccount_btn(driver).click();
			try {
				if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
					Login_Page.FirstCommande_Alert(driver).click();
				}}catch(Exception e) {
				}
			Login_Page.Register(driver).click();
			RegistrationForm.Email_fld(driver).sendKeys(email);
			RegistrationForm.Password_fld(driver).sendKeys(Data.Password);
			RegistrationForm.CompanyName_fld(driver).sendKeys(Data.Company);
			Select sel1=new Select(RegistrationForm.Title_Select(driver));
			sel1.selectByIndex(1);
			RegistrationForm.FirstName_fld(driver).sendKeys(Data.FirstName);
			RegistrationForm.LastName_fld(driver).sendKeys(Data.LastName);
			RegistrationForm.PhoneHome_fld(driver).sendKeys(Data.MobileFR);
			RegistrationForm.Adresse1_fld(driver).sendKeys(Data.Adresse);
			RegistrationForm.PostalCode_fld(driver).sendKeys(Data.PostalCode_FR);
			RegistrationForm.City_fld(driver).sendKeys(Data.City);
			RegistrationForm.Submit_btn(driver).click();
			
		}catch(Exception e) {
			System.out.println("Problème dans la création du compte e-shop.");
			throw(e);
		}	 
		   } 

}
