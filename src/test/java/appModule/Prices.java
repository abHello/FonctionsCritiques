package appModule;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObject.Basket;


public class Prices {
	static List<WebElement> lst=null; 
	static double price=0;
	
	public static double TotalProduct_HT(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(1).getText().substring(0, (lst.get(1).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix HT.");
			throw(e);
		}
		return price; 
	}
	
	
	public static double Total_TTC(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(14).getText().substring(0, (lst.get(14).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix TTC.");
			throw(e);
		}
		return price;
	}
	
	
	public static double Total_TTC_freeShipping(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(12).getText().substring(0, (lst.get(12).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix TTC.");
			throw(e);
		}
		return price;
	}
	
	
	
	public static double ShippingCost(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(4).getText().substring(0, (lst.get(4).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix de livraison.");
			throw(e);
		}
		return price;
	}
	
	
	public static double RajaGarantie(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(8).getText().substring(0, (lst.get(8).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix de la garantie raja.");
			throw(e);
		}
		return price;
	}
	
	
	public static double RajaGarantie_freeShipping(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(6).getText().substring(0, (lst.get(6).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix de la garantie raja.");
			throw(e);
		}
		return price;
	}
	
	
	public static double Total_HT(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(10).getText().substring(0, (lst.get(10).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix HT.");
			throw(e);
		}
		return price;
	}
	
	
	public static double Total_HT_freeShipping(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(8).getText().substring(0, (lst.get(8).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix HT.");
			throw(e);
		}
		return price;
	}
	
	
	public static double TVA(WebDriver driver) throws Exception{
		try {
			  lst=Basket.TablePrice_Recap(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(12).getText().substring(0, (lst.get(12).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du montant de la TVA.");
			throw(e);
		}
		return price;
	}
	
	
	public static double UnitPrice(WebDriver driver) throws Exception{
		try {
			  lst=Basket.Table_Ref(driver).findElements(By.tagName("td"));
			  price=Double.parseDouble(lst.get(3).getText().substring(0, (lst.get(3).getText().length()-2)).replace(",","."));

		}catch(Exception e) {
			System.out.println("Erreur : récupération du prix unitaire.");
			throw(e);
		}
		return price;
	}

}
