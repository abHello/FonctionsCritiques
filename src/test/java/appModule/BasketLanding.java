package appModule;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObject.Basket;
import pageObject.Home;
import pageObject.Login_Page;
import utility.Data;


public class BasketLanding {
	static List<WebElement> UsersNumber=null; 
	
	public static void Basket(WebDriver driver) throws Exception{

		try {
			Home.OrderQA(driver).click();
			try {
				if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
					Login_Page.FirstCommande_Alert(driver).click();
				}}catch(Exception e) {
				}
			Basket.RefProd(driver).sendKeys(Data.Commande_Ref);
			Thread.sleep(1000);
			UsersNumber=Basket.TableProduct(driver).findElements(By.tagName("td"));
			UsersNumber.get(0).click(); 
			Basket.AddToBasket_btn(driver).click();
			Basket.SeeBasket_btn(driver).click();
		
		}catch(Exception e) {
			System.out.println("BasketLanding n'a pas réussi."); 
			throw(e);
		}
		
		
	}

}
