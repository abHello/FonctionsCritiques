package appModule;


import org.openqa.selenium.WebDriver;

import pageObject.Login_Page;
import utility.Data;


public class Login {
	
	public static void login(WebDriver driver) throws Exception{
		try {
			
			try {
			if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
				Login_Page.FirstCommande_Alert(driver).click();
			}}catch(Exception e) {
			}
			Login_Page.Login_btn(driver).click();
			Login_Page.Email(driver).sendKeys(Data.Email);
			Login_Page.Password(driver).sendKeys(Data.Password);
			Login_Page.Submit(driver).click();
		}catch(Exception e) {
			System.out.println("Login au compte n'a pas réussi.");
			throw(e);
		} 
		
		
	}

}
