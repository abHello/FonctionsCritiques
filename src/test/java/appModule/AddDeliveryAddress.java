package appModule;

import org.openqa.selenium.WebDriver;

import pageObject.Delivery;
import utility.Data;
import utility.RandomString;


public class AddDeliveryAddress {
	
	public static void AddAddress(WebDriver driver) throws Exception{

		try {
			Delivery.AdressName(driver).sendKeys(RandomString.NameString());
			Delivery.CompanyName(driver).sendKeys(Data.Company);
			Delivery.NVoie(driver).sendKeys(Data.Adresse);
			Delivery.PostalCode(driver).sendKeys(Data.PostalCode_FR);
			Delivery.City(driver).sendKeys(Data.City);
			Delivery.Contact_FirstName(driver).sendKeys(Data.FirstName);
			Delivery.Contact_FLastName(driver).sendKeys(Data.LastName);
			Delivery.Contact_Email(driver).sendKeys(Data.Email);
			Delivery.Contact_Phone(driver).sendKeys(Data.MobileFR);
			Thread.sleep(1000);
			Delivery.SubmitAddress(driver).click();
			Delivery.ReturnToDelivery_btn(driver).click();
		}catch(Exception e) {
			System.out.println("erreur : création d'adresse de livraison.");
			throw(e);
		}
		
		
	}
}

